package corso.lez19.SpringHelloWorld.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import corso.lez19.SpringHelloWorld.models.Studente;

@RestController
@RequestMapping("/studente")
public class StudenteController {

//	@PostMapping("/inserisci")
//	public String inserisciStudente(@RequestBody Studente objStud){
//		return objStud.toString();
//	}
	
	@PostMapping("/inserisci")
	public Studente inserisciStudente(@RequestBody Studente objStud){
		return objStud;
	}
	
	@PostMapping("/lista")
	public List<Studente> listaStudenti(){
		
		Studente gio = new Studente("Giovanni", "Pace", "AB123456");
		Studente mar = new Studente("Mario", "Rossi", "AB123457");
		Studente val = new Studente("Valeria", "Verdi", "AB123458");
		
		List<Studente> elenco = new ArrayList();
		elenco.add(gio);
		elenco.add(mar);
		elenco.add(val);
		
		return elenco;
	}
	
	@PostMapping("/importa")
	public String importazioneStudenti(@RequestBody List<Studente> elenco) {
		
		for(int i=0; i<elenco.size(); i++) {
			Studente temp = elenco.get(i);
			System.out.println(temp.toString());
		}
		
		return "tutto ok";
	}
}
