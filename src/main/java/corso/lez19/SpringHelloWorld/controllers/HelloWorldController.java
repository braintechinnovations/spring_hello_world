package corso.lez19.SpringHelloWorld.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloWorldController {
	
	@GetMapping("/")					//Intercetto la REQUEST
	public String testFunzionamento() {
		
		System.out.println("Tutto sta funzionando corretamente!");
		
		return "Funziono!";				//Restituisce la RESPONSE
	}

	@GetMapping("/giovanni")			// http://localhost:8081/hello/giovanni
	public String salutaGiovanni() {
		return "Ciao Giovanni";
	}
	
	@GetMapping("/mario")
	public String salutaMario() {
		return "Ciao Mario";
	}
	
	@GetMapping("/saluta/{nome}")
	public String salutaNome(@PathVariable String nome) {
		return "Ciao " + nome;
	}
	
	@GetMapping("/saluta/{nome}/{cognome}")
	public String salutaNomeCognome(@PathVariable String nome, @PathVariable String cognome) {		
		return "Ciao " + nome + ", " + cognome;
	}
	
	@GetMapping("/saluta/{nome_per}/{cognome_per}")
	public String salutaNomeCognomeDue(@PathVariable(name="nome_per") String nome, @PathVariable(name="cognome_per") String cognome) {		
		return "Ciao " + nome + ", " + cognome;
	}
	
	@GetMapping("/persona")		// http://localhost:8081/hello/persona?nomePer=Giovanni&cognomePer=Pace
	public String salutaPersona(
			@RequestParam( name="nomePer" ) String nome, 
			@RequestParam( name="cognomePer" ) String cognome) {
		return "Ciao " + nome + ", " + cognome;
	}
	
	@GetMapping("/test")
	public String testGet() {
		return "Il get funziona! ;)";
	}
	
	//SEZIONE POST
	
	@PostMapping("/test")
	public String testPost() {
		return "Il post funziona! ;)";
	}
	
	@RequestMapping("/requesttest")
	public String testUniversale() {
		return "Sto ritornando qualcosa...";
	}

	//URL = BASE_PATH + SEGMENTS
	
}
